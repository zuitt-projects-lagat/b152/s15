let user1 = {
	username: "jackscepticeye",
	email: "seanScepticeye@gmail.com",
	age: 31,
	topics: ["Gaming","Commentary"]
}

console.log(user1);

function isLegalAge(user){
	if (user.age >= 18){
		console.log('Welcome to the Club.')
	} else {
		console.log('You cannot enter the club yet.')
	}
}

isLegalAge(user1);

function addTopic(topic){
	if (topic.length >= 5){
		user1.topics.push(topic);
		console.log(`${topic} has been added to topics.`)
	} else {
		console.log("Enter a topic with at least 5 characters.")
	}
}

addTopic("Love");
addTopic("Comedy");
console.log("New list of topics: ",user1.topics);

const clubMembers = ["jackscepticeye","pewdiepie"];

function register(member){
	if (member.length >= 8){
		clubMembers.push(member)
		console.log(`"${member}" has been added to the Club Members`)
	} else {
		console.log('Please enter a username longer or equal to 8 characters.')
	}
}

register('shofu');
register('markiplier');
console.log("New list of club members :",clubMembers);